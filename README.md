# FARSYNC

Uses fswatch and rsync to keep multiple directories on a remote filesystem in sync with a local copy. 

## Install
```
npm install -g git+https://bitbucket.org/dynosmurf/farsync.git
```

## Usage

Place a json file in the root of your local project directory. This file must be named `.farsync.json`. See an example in example.config.json.

Once this file is in place run, `farsync run`. 

This will do a first time sync of the local to the remote directories. Then run `farsync start`. 

## TODO
- Cleanup console logging. 

