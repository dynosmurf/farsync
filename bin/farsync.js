#!/usr/bin/env node

var exec = require('child_process').exec;
var args = require('minimist')(process.argv.slice(2));
var path = require('path')
var sane = require('sane')


var CONFIG_FILE_NAME = "./.farsync.json";

var first_arg = function(){
    return args._[0];
}
var first_arg_is = function(key) {
    if (args._[0] && args._[0] == key) {
        return true;
    }
    return false;
}

const usage = `
FAR Sync v 0.0.2 
  options: 
      start  
          start deamon to monitor file changes 
      run  
          stop deamon  
      init  
          create configuration file in current directory`

const promiseFromChildProcess = function(child) {
  return new Promise(function (resolve, reject) {
    child.addListener("error", reject);
    child.addListener("exit", resolve);
  });
}

const execPromise = function(command) {
  return new Promise( (resolve, reject) => {
    exec(command, function(err, stdout, stderr) {
      if(err){
        reject(err)
      } else {
        resolve( { stdout, stderr} )
      }
    })
  })
}

const buildSynccommand = function(conf, source, dest) {
  var out = [];
  out.push('rsync');
  out.push("-e ssh");

  var exclude = (conf.exclude || []).map( function(e) {
    return "--exclude='" + e + "'";
  }).join(' ');


  out.push(exclude);

  var flags = conf.rsync.flags.join(' ');
  out.push(flags);

  out.push(source);
  out.push(dest.host + ':' + dest.dir);

  return out.join(' ');
}

/**
 *
 * source is relative system path
 * dest is rsync remote location with path
 * cmd is command to run on local files before sync. 
 */
var handleChanges = async function(conf, target, cmd) {
  var command = buildSynccommand(conf, target.source, target.dest)

  console.log("RUNNING -> ", command)
  var result = await execPromise(command)

  process.stdout.write(result.stdout)
}

var startWatcher = function(conf, target, cmd) {
  var status = "idle" // running, dirty  
  var handler = () => handleChanges(conf, target, cmd)

  var watcher = sane(target.source);

  watcher.on('ready', function () { 
    console.log(`WATCHING : ${target.source}`);
  });

  watcher.on('change', function (filepath, root, stat) { 
    handler()
  });

  watcher.on('add', function (filepath, root, stat) { 
    handler()
  });

  watcher.on('delete', function (filepath, root) { 
    handler()
  });
}

// if init add config file to current directory.
if (args.h) {
  console.log(usage);
  process.exit();
}




var main = function(){

  var config_file = path.join(process.cwd(), CONFIG_FILE_NAME);
  var conf = require(config_file)

  // always exclude farsync conf.
  conf.exclude.push(".farsync*")

  console.log(conf);

  console.log('##############\nFARSYNC : \n##############\n');

  if (first_arg_is('start')) {

    conf.mappings.map( async function(mapping){
      // sync all changes
      await handleChanges(conf, mapping, "")
      // watch for new changes
      startWatcher(conf, mapping, "")
    });

  } else if (first_arg_is('run')) {

    conf.mappings.map(function(mapping){
      handleChanges(conf, mapping, "") 
    });

  } else if (first_arg_is('log')) {

    // XXX 

  }
}


main();
